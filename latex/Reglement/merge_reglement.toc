\select@language {german}
\contentsline {section}{\numberline {I}Reglement der Hochschulpolitikkommission}{2}
\contentsline {subsection}{\numberline { Art. 2\global \advance \c@article 1\relax }Name}{2}
\contentsline {subsection}{\numberline { Art. 3\global \advance \c@article 1\relax }T\IeC {\"a}tigkeit}{2}
\contentsline {subsection}{\numberline { Art. 4\global \advance \c@article 1\relax }Mitgliedschaft}{2}
\contentsline {subsection}{\numberline { Art. 5\global \advance \c@article 1\relax }Organisation}{3}
\contentsline {subsection}{\numberline { Art. 6\global \advance \c@article 1\relax }Finanzen}{3}
\contentsline {subsection}{\numberline { Art. 7\global \advance \c@article 1\relax }Schlussbestimmungen}{3}
\contentsline {section}{\numberline {II}Reglement der Party- und Kulturkommission}{4}
\contentsline {subsection}{\numberline { Art. 2\global \advance \c@article 1\relax }Name}{4}
\contentsline {subsection}{\numberline { Art. 3\global \advance \c@article 1\relax }T\IeC {\"a}tigkeit}{4}
\contentsline {subsection}{\numberline { Art. 4\global \advance \c@article 1\relax }Mitgliedschaft}{4}
\contentsline {subsection}{\numberline { Art. 5\global \advance \c@article 1\relax }Organisation}{5}
\contentsline {subsection}{\numberline { Art. 6\global \advance \c@article 1\relax }Finanzen}{5}
\contentsline {subsection}{\numberline { Art. 7\global \advance \c@article 1\relax }Schlussbestimmungen}{5}
\contentsline {section}{\numberline {III}Reglement der BAM-Kommission}{6}
\contentsline {subsection}{\numberline { Art. 2\global \advance \c@article 1\relax }Name}{6}
\contentsline {subsection}{\numberline { Art. 3\global \advance \c@article 1\relax }T\IeC {\"a}tigkeit}{6}
\contentsline {subsection}{\numberline { Art. 4\global \advance \c@article 1\relax }Mitgliedschaft}{6}
\contentsline {subsection}{\numberline { Art. 5\global \advance \c@article 1\relax }Organisation}{7}
\contentsline {subsection}{\numberline { Art. 6\global \advance \c@article 1\relax }Finanzen}{7}
\contentsline {subsection}{\numberline { Art. 7\global \advance \c@article 1\relax }Schlussbestimmungen}{8}
\contentsline {section}{\numberline {IV}Reglement der Nijmegenkommission}{9}
\contentsline {subsection}{\numberline { Art. 2\global \advance \c@article 1\relax }Name}{9}
\contentsline {subsection}{\numberline { Art. 3\global \advance \c@article 1\relax }T\IeC {\"a}tigkeit}{9}
\contentsline {subsection}{\numberline { Art. 4\global \advance \c@article 1\relax }Mitgliedschaft}{9}
\contentsline {subsection}{\numberline { Art. 5\global \advance \c@article 1\relax }Organisation}{10}
\contentsline {subsection}{\numberline { Art. 6\global \advance \c@article 1\relax }Finanzen}{10}
\contentsline {subsection}{\numberline { Art. 7\global \advance \c@article 1\relax }Schlussbestimmungen}{10}
\contentsline {section}{\numberline {V}Reglement der Party- und Kulturkommission}{11}
\contentsline {subsection}{\numberline { Art. 2\global \advance \c@article 1\relax }Name}{11}
\contentsline {subsection}{\numberline { Art. 3\global \advance \c@article 1\relax }T\IeC {\"a}tigkeit}{11}
\contentsline {subsection}{\numberline { Art. 4\global \advance \c@article 1\relax }Mitgliedschaft}{11}
\contentsline {subsection}{\numberline { Art. 5\global \advance \c@article 1\relax }Organisation}{12}
\contentsline {subsection}{\numberline { Art. 6\global \advance \c@article 1\relax }Finanzen}{12}
\contentsline {subsection}{\numberline { Art. 7\global \advance \c@article 1\relax }Schlussbestimmungen}{12}
\contentsline {section}{\numberline {VI}Reglement der Industriekommission}{13}
\contentsline {subsection}{\numberline { Art. 2\global \advance \c@article 1\relax }Name}{13}
\contentsline {subsection}{\numberline { Art. 3\global \advance \c@article 1\relax }T\IeC {\"a}tigkeit}{13}
\contentsline {subsection}{\numberline { Art. 4\global \advance \c@article 1\relax }Mitgliedschaft}{13}
\contentsline {subsection}{\numberline { Art. 5\global \advance \c@article 1\relax }Organisation}{14}
\contentsline {subsection}{\numberline { Art. 6\global \advance \c@article 1\relax }Finanzen}{14}
\contentsline {subsection}{\numberline { Art. 7\global \advance \c@article 1\relax }Schlussbestimmungen}{14}
