\select@language {german}
\contentsline {section}{\numberline {I}Name, Zweck}{2}
\contentsline {subsection}{\numberline { Art. 76\global \advance \c@article 1\relax }Name}{2}
\contentsline {subsection}{\numberline { Art. 77\global \advance \c@article 1\relax }Zweck, T\IeC {\"a}tigkeit}{2}
\contentsline {subsection}{\numberline { Art. 78\global \advance \c@article 1\relax }Gesch\IeC {\"a}ftsjahr}{2}
\contentsline {section}{\numberline {II}Mitglieder}{3}
\contentsline {subsection}{\numberline { Art. 79\global \advance \c@article 1\relax }Mitgliedschaft}{3}
\contentsline {subsection}{\numberline { Art. 80\global \advance \c@article 1\relax }Rechte}{3}
\contentsline {subsection}{\numberline { Art. 81\global \advance \c@article 1\relax }Pflichten}{3}
\contentsline {subsection}{\numberline { Art. 82\global \advance \c@article 1\relax }Austritt und Ausschluss}{3}
\contentsline {subsection}{\numberline { Art. 83\global \advance \c@article 1\relax }Mitgliederbeitrag}{4}
\contentsline {section}{\numberline {III}Finanzen}{4}
\contentsline {subsection}{\numberline { Art. 84\global \advance \c@article 1\relax }Mittel}{4}
\contentsline {subsection}{\numberline { Art. 85\global \advance \c@article 1\relax }Haftung}{4}
\contentsline {section}{\numberline {IV}Organe}{5}
\contentsline {subsection}{\numberline { Art. 86\global \advance \c@article 1\relax }Organe}{5}
\contentsline {subsection}{\numberline { Art. 87\global \advance \c@article 1\relax }Ordentliche Generalversammlung \sout {(GV)}}{5}
\contentsline {subsection}{\numberline { Art. 88\global \advance \c@article 1\relax }Ausserordentliche Generalversammlung}{5}
\contentsline {subsection}{\numberline { Art. 89\global \advance \c@article 1\relax }Einberufung und Abhaltung}{6}
\contentsline {subsection}{\numberline { Art. 90\global \advance \c@article 1\relax }Wahlen und Abstimmungen}{6}
\contentsline {subsection}{\numberline { Art. 91\global \advance \c@article 1\relax }Berechnung von Mehrheiten}{7}
\contentsline {subsection}{\numberline { Art. 92\global \advance \c@article 1\relax }Gesch\IeC {\"a}fte}{7}
\contentsline {section}{\numberline {V}Vorstand}{8}
\contentsline {subsection}{\numberline { Art. 93\global \advance \c@article 1\relax }Mitglieder}{8}
\contentsline {subsection}{\numberline { Art. 94\global \advance \c@article 1\relax }Aufgaben}{9}
\contentsline {subsection}{\numberline { Art. 95\global \advance \c@article 1\relax }Vorstandssitzungen}{9}
\contentsline {subsection}{\numberline { Art. 96\global \advance \c@article 1\relax }Konstituierung}{9}
\contentsline {subsection}{\numberline { Art. 97\global \advance \c@article 1\relax }Finanzen}{10}
\contentsline {subsection}{\numberline { Art. 98\global \advance \c@article 1\relax }Entsch\IeC {\"a}digung}{10}
\contentsline {section}{\numberline {VI}Kommissionen}{10}
\contentsline {subsection}{\numberline { Art. 99\global \advance \c@article 1\relax }Grundlage}{10}
\contentsline {subsection}{\numberline { Art. 100\global \advance \c@article 1\relax }Kommissionsreglement}{11}
\contentsline {subsection}{\numberline { Art. 101\global \advance \c@article 1\relax }Mitglieder}{11}
\contentsline {subsection}{\numberline { Art. 102\global \advance \c@article 1\relax }Organisation}{11}
\contentsline {subsection}{\numberline { Art. 103\global \advance \c@article 1\relax }Finanzen}{11}
\contentsline {section}{\numberline {VII}Rechnungsrevisoren}{11}
\contentsline {subsection}{\numberline { Art. 104\global \advance \c@article 1\relax }Zusammensetzung}{11}
\contentsline {subsection}{\numberline { Art. 105\global \advance \c@article 1\relax }Aufgabe}{12}
\contentsline {section}{\numberline {VIII}Vertretungen in anderen Organisationen}{12}
\contentsline {subsection}{\numberline { Art. 106\global \advance \c@article 1\relax }Vertretungen}{12}
\contentsline {subsection}{\numberline { Art. 107\global \advance \c@article 1\relax }Vertretungen im D-CHAB}{12}
\contentsline {subsection}{\numberline { Art. 108\global \advance \c@article 1\relax }Vertretungen im VSETH}{12}
\contentsline {subsection}{\numberline { Art. 109\global \advance \c@article 1\relax }Berichterstattung}{13}
\contentsline {section}{\numberline {IX}Schlussbestimmungen}{13}
\contentsline {subsection}{\numberline { Art. 110\global \advance \c@article 1\relax }Statuten}{13}
\contentsline {subsection}{\numberline { Art. 111\global \advance \c@article 1\relax }Vereinsaufl\IeC {\"o}sung}{13}
\contentsline {subsection}{\numberline { Art. 112\global \advance \c@article 1\relax }Inkraftsetzung}{13}
